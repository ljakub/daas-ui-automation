Feature: Login Logout DaaS Application Scenarios
  Background: Navigate to the DaaS application
    Given Navigate to Daas url

  @E2E
  Scenario: Empty credentials - unsuccessful Login to DaaS TestcaseIds
    When Login using user name "" and password ""
    And I click to Sign in button
    Then "Email" validation error is
    """
    Email is required.
    """
    And "Password" validation error is
    """
    Password is required.
    """

  @E2E
  Scenario: Invalid email - unsuccessful Login to DaaS TestcaseIds
    When Login using user name "invalid4@catalina.com" and password "Catalina123"
    And I click to Sign in button
    Then "Account" validation error is
    """
    Sorry your account could not be validated. Please check your email and password and try again. If you still are having issues please contact support.
    """

  @E2E
  Scenario: Invalid password - unsuccessful Login to DaaS TestcaseIds
    When Login using user name "cs.auto4@catalina.com" and password "Catalina333"
    And I click to Sign in button
    Then "Account" validation error is
    """
    Sorry your account could not be validated. Please check your email and password and try again. If you still are having issues please contact support.
    """

  @E2E
  Scenario: Successful Login to DaaS TestcaseIds
    When Login using default credentials
    Then Contracts & Delivery page is displayed successfully
    Then Logout of the application
