Feature: Create Contract DaaS Application Scenarios
  Background: Navigate to the DaaS application
    Given Navigate to Daas url
    When Login using default credentials
    Then Contracts & Delivery page is displayed successfully
    When I navigate to create contract page

  @E2E
  Scenario: Create contract - values for mandatory fields missing TestcaseIds
    And I click on "Save" button
    Then Validation errors appear
      """
      Missing Client Name.
      Missing Client Type.
      Missing Data Feed Type.
      Missing Delivery Cadence.
      Missing Start Date.
      Missing End Date.
      You are missing Networks
      You are missing UPCs
      You are missing Delivery Location
      You are missing Initial Delivery Date
      Missing Final ID Type Delivered.
      The Campaign Start Date should not be less than the Campaign End Date
      """
    And Logout of the application

  @E2E @removeContract
  Scenario: Update contract - provide values for missing mandatory fields TestcaseIds
    And  Title "Contract Create" is displayed
    And I "enter" the following contract details
      |Field                    |Value              |
      |Client Name              |Client Name        |
      |Client Type              |Non Reseller       |
      |Data Feed Type           |Campaign Sales Feed|
      |Delivery Cadence         |Weekly             |
      |Start Date               |01/02/2020         |
      |End Date                 |01/01/2020         |
      |Delivery Location        |AmazonS3           |
      |Final ID Type Delivered  |Amobee             |
      |Initial Delivery Date    |10/22/2020         |
      |UPCs                     |365436536536, 5454674643 |
    And I "select" all networks
    When I "update" the following contract details
      |Field                    |Value              |
      |UPCs                     | |
      |Client Name              | |
    And I "deselect" all networks
    And I click on "Save" button
    Then Validation errors appear
      """
      Missing Client Name.
      You are missing Networks
      You are missing UPCs
      The Campaign Start Date should not be less than the Campaign End Date
      """
    When I "update" the following contract details
      |Field                    |Value              |
      |Start Date               |01/02/2020         |
      |End Date                 |01/05/2020         |
      |UPCs                     |6905582420,1500000310,3700081867,4111620601,7080004146,4400004265,4119601013,30045018404,5100027158,4141942008|
      |Client Name              |Updated Client Name |
    And I "select" all networks
    And I click on "Save" button
    Then Title "Contracts" is displayed
    And Logout of the application

  @E2E @removeContract
  Scenario: Create contract - all the mandatory values provided TestcaseIds
     And  Title "Contract Create" is displayed
     And I "enter" the following contract details
      |Field                    |Value              |
      |Client Name              |client test 777    |
      |Client Type              |Non Reseller       |
      |Data Feed Type           |Campaign Sales Feed|
      |Delivery Cadence         |Weekly             |
      |Start Date               |01/01/2020         |
      |End Date                 |01/07/2020         |
      |Delivery Location        |AmazonS3           |
      |Final ID Type Delivered  |Amobee             |
      |Initial Delivery Date    |01/08/2020         |
      |UPCs                     |6905582420,1500000310,3700081867,4111620601 |
    And I "select" all networks
    And I click on "Save" button
    Then Title "Contracts" is displayed
    And I verify contract with Client name "client test 777" is present
    And I verify contract with Client name "client test 777" has "Start Date" "01/01/2020"
    And I verify contract with Client name "client test 777" has "End Date" "01/07/2020"
    And Logout of the application


