Feature: Contracts Table DaaS Application Scenarios
  Background: Navigate to the DaaS application
    Given Navigate to Daas url

  @E2E
  Scenario: Contracts table verification TestcaseIds
    When Login using default credentials
    Then Contracts & Delivery page is displayed successfully
    And Contracts table contains the following columns
    |Contract Id| Client Name|Start Date|End Date|Created Date|
    And Logout of the application

  @E2E @removeContract
  Scenario: Contract details verification TestcaseIds
    When Login using default credentials
    Then Contracts & Delivery page is displayed successfully
    And I navigate to create contract page
    And  Title "Contract Create" is displayed
    And I "enter" the following contract details
      |Field                    |Value              |
      |Client Name              |test+client+name   |
      |Client Type              |Non Reseller       |
      |Data Feed Type           |Campaign Sales Feed|
      |Delivery Cadence         |Weekly             |
      |Start Date               |01/01/2020         |
      |End Date                 |01/02/2020         |
      |Delivery Location        |AmazonS3           |
      |Final ID Type Delivered  |Amobee             |
      |Initial Delivery Date    |10/22/2020         |
      |UPCs                     |6905582420,1500000310,3700081867,4111620601 |
    And I "select" all networks
    And I click on "Save" button
    Then Title "Contracts" is displayed
    And I verify contract with Client name "test+client+name" is present
    And I verify contract with Client name "test+client+name" has "Start Date" "01/01/2020"
    And I verify contract with Client name "test+client+name" has "End Date" "01/02/2020"
    When I click to edit contract with Client Name "test+client+name"
    And  Title "Contract Edit" is displayed
    And I "update" the following contract details
      |Field                    |Value              |
      |Start Date               |02/05/2020         |
      |End Date                 |10/10/2020         |
    And I click on "Save" button
    Then Title "Contracts" is displayed
    And I verify contract with Client name "test+client+name" is present
    And I verify contract with Client name "test+client+name" has "Start Date" "02/05/2020"
    And I verify contract with Client name "test+client+name" has "End Date" "10/10/2020"
    And Logout of the application
