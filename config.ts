import { browser, Config } from "protractor";
import { Reporter } from "./util/Reporter";
const jsonReports = process.cwd() + "/reports/json";
const path = require('path')
var downloadsPath = path.resolve(__dirname, '../downloads');
export let config: Config = {
  allScriptsTimeout: 10000000000,
  directConnect: true,
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  baseUrl: 'http://digital-daas-dashboard-om-sqa.nplabsusk8s.catmktg.com/',
  // baseUrl: 'http://localhost:9000',

  SELENIUM_PROMISE_MANAGER: false,
  /**
     * If true, protractor will restart the browser between each test. Default
     * value is false.
     *
     * CAUTION: This will cause your tests to slow down drastically.
     */
  // restartBrowserBetweenTests: true,

  params: {
    signIn: {
      email: 'cs.auto4@catalina.com',
      password: 'Catalina123'
    }

  },

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: "chrome",
    chromeOptions: {
      // --allow-file-access-from-files - allow XHR from file://
      args: ['allow-file-access-from-files'],
      prefs: {
        download: {
          'prompt_for_download': false,
          'directory_upgrade': true,
          'default_directory': downloadsPath
        },
      }
    }
  },

  onPrepare: () => {
    browser.driver
      .manage()
      .window()
      .maximize();
    browser.ignoreSynchronization = true;
    Reporter.createDirectory(jsonReports);
  },

  specs: ["../features/*.feature"],

  cucumberOpts: {
    compiler: "ts:ts-node/register",
    format: "json:./reports/json/cucumber_report.json",
    require: ["./util/timeout.js", "./stepDefinitions/*.js"],
    strict: true,
   tags: ['@removeContract or @E2E']


  },

  onComplete: () => {
    Reporter.createHTMLReport();
  }
};
