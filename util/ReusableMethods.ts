import { protractor } from "protractor/built/ptor";
import { browser } from "protractor";

export class ReusableMethods{
    async customWait(elem){
        var EC=protractor.ExpectedConditions;
        var Elem_displayed=EC.visibilityOf(elem);
        await browser.wait(Elem_displayed,10000);
    }
}