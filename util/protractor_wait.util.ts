import {browser, ExpectedConditions} from 'protractor';
import 'regenerator-runtime/runtime';

export class protractorWaitUtil {
  timeout;

  constructor() {
    this.timeout = 30; // in seconds
  }

  async waitForElementToBeVisible(element) {
    await browser.wait(ExpectedConditions.visibilityOf(element), this.timeout * 1000, element.locator() + ' taking more than ' + this.timeout + ' to be visible.');
  }

  async waitMaxFiveMinForElementToBeVisible(element) {
    await browser.wait(ExpectedConditions.visibilityOf(element), this.timeout * 20000, element.locator() + ' taking more than ' + this.timeout + ' to be visible.');
  }

  async waitForElementToBeInVisible(element) {
    await browser.wait(ExpectedConditions.invisibilityOf(element), this.timeout * 1000, element.locator() + ' taking more than ' + this.timeout + ' to be invisible.');
  }

  async waitMaxFiveMinForElementToBeInVisible(element) {
    await browser.wait(ExpectedConditions.invisibilityOf(element), this.timeout * 20000, element.locator() + ' taking more than ' + this.timeout + ' to be invisible.');
  }

  async waitForElementToBeClickable(element) {
    await browser.wait(ExpectedConditions.elementToBeClickable(element), this.timeout * 1000, element.locator() + ' taking more than ' + this.timeout + ' to be clickable.');
  }

  async isElementClickable(element) {
    try {
      await browser.wait(ExpectedConditions.elementToBeClickable(element), this.timeout * 1000, element.locator() + ' is not clickable');
      return true;
    } catch (Exception) {
      return false;
    }
  }

  async waitForElementAttribute(element, attribute, value) {
    await browser.wait(async () => {
      const result = await element.getAttribute(attribute);
      return result === value;
    }, this.timeout * 1000, element.locator() + ' attribute is not present' );
  }

  // Function is waiting for first element in drop down list
  async tryWaitForElementInList(list) {
    let currentCount = 0;
    const maxCount = 3;
    let finish = false;
    do {
      try {
        await browser.wait(async () => {
          let result = await list.first().getText();
          finish = (result !== 'Loading results…') && (result !== 'Searching…');
          return finish;
        }, 5000);
      } catch (error) {
        currentCount++;
      }
    } while (currentCount < maxCount && !finish);
  }

  async isElementVisible(element) {
    try {
      await browser.wait(ExpectedConditions.visibilityOf(element), 3000, element.locator() + ' is not visible');
      return true;
    } catch (Exception) {
      return false;
    }
  }

  // Function is waiting for searched element in drop down list
  async tryWaitForSearchedElement(element, text) {
    let currentCount = 0;
    const maxCount = 6;
    let finish = false;
    do {
      try {
        await browser.wait(async () => {
          let result = await element.first().getText();
          finish = (result.includes(text)) && (result !== 'Searching…');
          return finish;
        }, 5000);
      } catch (error) {
        currentCount++;
      }
    } while (currentCount < maxCount && !finish);
  }

  // Function is trying to search and click correct element in multiple drop down list
  async trySelectSearchedElement(elements, text) {
    let currentCount = 0;
    const maxCount = 6;
    let finish = false;
    do {
      try {
        await browser.wait(async () => {
          for (let i = 0; i <= await elements.count(); i++) {
            let result = await elements.get(i).getText();
            if (result === text) {
              await elements.get(i).click();
              return finish;
            }
          }
        }, 5000);
      } catch (error) {
        currentCount++;
      }
    } while (currentCount < maxCount && !finish);
  }

  async waitForElementToBeAttached(element) {
    let currentCount = 0;
    const maxCount = 6;
    let finish = false;
    do {
      try {
        await browser.wait(ExpectedConditions.presenceOf(element), this.timeout * 1000, element.locator() + ' taking more than ' + this.timeout + ' to be attached to the page document.');
        return finish;
      } catch (error) {
        currentCount++;
      }
    } while (currentCount < maxCount && !finish);
  }

  async waitForTextToBePresentInElement(element, text) {
    await browser.wait(ExpectedConditions.textToBePresentInElement(element, text), this.timeout * 1000, 'Text taking more than ' + this.timeout + ' to be present in ' + element.locator());
  }

  async waitForElementNotToBeAttached(element) {
    await browser.wait(ExpectedConditions.stalenessOf(element), this.timeout * 1000, element.locator() + ' taking more than ' + this.timeout + ' not to be attached to the page document.');
  }

  async waitForDropDownToBeEnabled(dropDownElement) {
    await browser.wait(async () => {
      return await dropDownElement.getAttribute('tabindex') === '0';
    }, this.timeout * 3000, 'Dropdown - ' + dropDownElement.locator() + ' is not enabled');
  }

  async waitForAnyText(element) {
    await browser.wait(async () => {
      const result = await element.getText();
      return result !== '';
    }, this.timeout * 1000, 'Any text taking more than ' + this.timeout + ' to be present in ' + element.locator());
  }

  async waitForTitleToChange(value) {
    await browser.wait(async() => {
      const title = await browser.getTitle();
      return value === title;
    }, this.timeout * 3000, 'Title change is taking more than ' + this.timeout * 3000  + 'expected value: ' + value + ' current value: ' + await browser.getTitle());
  }
}
