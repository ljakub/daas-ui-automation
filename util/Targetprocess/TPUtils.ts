import * as inputdata from '../../inputData.json';
import * as httpm from "typed-rest-client/HttpClient";
import logger from '../logger.log';

export class createBuildinTP {
//To Create the build in TP
    async postBuildName(Data) {
        let _http = new httpm.HttpClient("typed-test-client-tests", [], {
            headers: {
                'Content-Type': 'application/json',
                'Accept':'application/json'
            }
        });
        logger.info("PostbuildName method started");
        let url = inputdata.TargetProcess.hostName + "/TestPlanRuns?access_token=" + inputdata.TargetProcess.token;
        logger.info(url);
        let res: httpm.HttpClientResponse = await _http.post(url,Data);
        logger.info(res.message.statusCode + "::" + res.message.statusMessage);
        var JsonResponse:any = await JSON.parse(await res.readBody());
        logger.info(JsonResponse);
        logger.info("Build Created");
        return JsonResponse;
    }

    //To get the build details
    async getBuildDetails(testPlanID,buildName){
        logger.info("getbuild details method started");
        let _http = new httpm.HttpClient("typed-test-client-tests", [], {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        let url = inputdata.TargetProcess.hostName +"/TestPlans/"+testPlanID+"/TestPlanRuns?access_token="+inputdata.TargetProcess.token+" &where=(name eq '"+buildName+"')"   
        logger.info(url);
        let res: httpm.HttpClientResponse = await _http.get(url);
        logger.info(res.message.statusCode + "::" + res.message.statusMessage);
        var JsonResponse:any = await JSON.parse(await res.readBody());
        return JsonResponse;
    }

    //To Create the build if there is no build present
    async createBuild(buildName,testPlanID){
        var  Data = `{"TestPlan": {"Id": ${testPlanID}},"name":\"${buildName}\"}`;
        logger.info(Data);
        var resp:any=await this.getBuildDetails(testPlanID,buildName);
        const resp1=resp.Items[0];
        logger.info(resp1);
        if(resp1!=null){
            logger.info("Build Found")
        }else{
            logger.info("Build not found");
            await this.postBuildName(Data);
        }
    }

 //For getting test case run id  
    async getTCRunID(TC_ID,buildName,testPlanID){
        var resp:any=await this.getBuildDetails(testPlanID,buildName);
        const build_id=resp.Items[0].Id;
        logger.info("getTCRunID method started");
        let _http = new httpm.HttpClient("typed-test-client-tests", [], {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        let url = inputdata.TargetProcess.hostName +"/TestCaseRuns/?access_token="+inputdata.TargetProcess.token+ "&where=(TestCase.ID eq " + TC_ID + ") and(TestPlanRun.ID eq " + build_id + ")"
        logger.info(url);
        let res: httpm.HttpClientResponse = await _http.get(url);
        logger.info(res.message.statusCode + "::" + res.message.statusMessage);
        var JsonResponse:any = await JSON.parse(await res.readBody());
        logger.info("test case run id::"+JsonResponse.Items[0].Id)
        return JsonResponse.Items[0].Id;
    }
    
//To update test case result
    async updateStatus(TC_Id,testplan_id,status){
        var dateFormat = require('date-format');
        const buildName = inputdata.TargetProcess.ApplicationBuildName + dateFormat("_MM_dd_yyyy", new Date());
        await this.createBuild(buildName,testplan_id);
        var testcaseRun_id=await this.getTCRunID(TC_Id,buildName,testplan_id);
        logger.info("Update status method started");
        let _http = new httpm.HttpClient("typed-test-client-tests", [], {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        let url = inputdata.TargetProcess.hostName +"/TestCaseRuns/"+testcaseRun_id+"?access_token="+inputdata.TargetProcess.token;
        logger.info(url);
        const body=`{"status": "${status}"}`;
        logger.info("status Body::"+body);
        let res: httpm.HttpClientResponse = await _http.post(url,body);
        logger.info(res.message.statusCode + "::" + res.message.statusMessage);
        var JsonResponse:any = await JSON.parse(await res.readBody());
        logger.info(JsonResponse);
    }
}