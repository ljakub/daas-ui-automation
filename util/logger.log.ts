'use strict';
const fs = require('fs');
const path = require('path');
const logDir = 'logger/logtrace';
const filename = path.join(logDir, 'results.log');

import * as winston from "winston";


const env = 'production';

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.json(),
  ),
  level: "info",
  transports: [
    new winston.transports.Console({
      level: 'info',
      format: winston.format.combine(
        winston.format.label({ label: path.basename(process.mainModule.filename) }), 
        winston.format.colorize(),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.printf(
          info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
        )
      )
    }),
    new winston.transports.File({
      filename: filename,
      maxsize: 20000,
    }),
  ],
});

// if (process.env.NODE_ENV !== "production") {
//   logger.add(new winston.transports.Console({
//     format: winston.format.simple(),
//   }));
// }

export default logger;