import {property} from "./property.util";
import logger from "./logger.log";

const fetch = require("node-fetch");
export class GenericService{


    constructor() {
    }

    async get(path) {
        return await this.command('GET', path );
    }

    async delete(path) {
        return await this.command('DELETE', path);
    }

    async post(path, body) {
        return await this.commandBody( 'POST', path, body);
    }

    async put(path, body) {
        return await this.commandBody('PUT', path,body);
    }

    async command(method, path) {
        let url = property.url + path;
        logger.info(method.toUpperCase() + ' URL :' + url);
        return await fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*'
                }
            });
    }

    async commandBody(method, path, body) {
        let url = property.url + path;
        logger.info('URL :' + url);
        const response = await fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Accept' : '*/*'
            },
            body:body
        });
        if (response.status !== 200) {
            console.log('Response status was: '+ response.status);
            console.log('Response was: '+ await response.text());
        } else {
            console.log('Response was: '+ await response.text());
            return response.status;
        }
    }
}

