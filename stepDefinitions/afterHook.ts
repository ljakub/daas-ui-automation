import {After, Status} from "cucumber";
import {browser} from "protractor";
import logger from "../util/logger.log";
import {GenericService} from "../util/genericService";
import {createContractPageImpl} from "../pageImplementation/createContractPageImpl";
import {createBuildinTP} from "../util/Targetprocess/TPUtils";

After( {tags: '@removeContract'},async function(scenario) {
    let name: string = scenario.pickle.name;
    await browser.sleep(3000);
    const contractId = (await new createContractPageImpl().returnContractId()).split(' ')[1];
    logger.info('Contract to be removed : '+ contractId);

    //DELETE contract
    const deleteStatus = await new GenericService().delete('contractanddelivery/'+contractId);
    if(deleteStatus != 200){
        logger.info('Scenario : '+ name +'. Delete status was : '+ deleteStatus.status);
    }

    //GET contract
    const getStatus = await new GenericService().get('contractanddelivery/'+contractId);
    if(getStatus.status === 200) {
        logger.info('Contract was not deleted');
    }
    // const getJsonObject = await JSON.parse(await getStatus.text());
    // await strictEqual(await getJsonObject.id, contractId, ' Get response body was :'+ getJsonObject);

    // expect(getStatus).to.be.a(404);
    // if (scenario.result.status === Status.FAILED) {
    //     // screenShot is a base-64 encoded PNG
    //     const screenShot = await browser.takeScreenshot();
    //     await this.attach(screenShot, "image/png");
    // }
});

// After(async function (scenario) {
//     let name: string = scenario.pickle.name;
//     logger.info("Scenario Name::" + name);
//     //This part is for Screenshot
//     if (scenario.result.status === Status.FAILED) {
//         // screenShot is a base-64 encoded PNG
//         const screenShot = await browser.takeScreenshot();
//         await this.attach(screenShot, "image/png");
//     }
//     //Below code is for Target process update result.
//     if (name.includes("TestcaseIds")) {
//         let arr: string[] = name.replace(" ", "").split("TestcaseIds");
//         var tp = new createBuildinTP();
//         logger.info("Testcase Ids ::" + arr[1]);
//         if (arr[1].includes(",")) {
//             let TC_ids: string[] = arr[1].split(",");
//             for (let j = 0; j < TC_ids.length; j++) {
//                 console.log(TC_ids[j]);
//                 await tp.updateStatus(TC_ids[j], inputdata.TargetProcess.MasterTestPlanID, scenario.result.status);
//             }
//         }
//         else {
//             await tp.updateStatus(arr[1], inputdata.TargetProcess.MasterTestPlanID, scenario.result.status);
//         }
//
//         logger.info("Test case result updated in Targetprocess");
//     }
//
//     await browser.executeScript('window.sessionStorage.clear();');
//     await browser.executeScript('window.localStorage.clear();');
// });