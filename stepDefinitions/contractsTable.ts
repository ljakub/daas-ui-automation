import {Then} from "cucumber";
import { expect } from 'chai';
import {contractsPageImpl} from "../pageImplementation/contractsPageImpl";
import {commonActionsImpl} from "../pageImplementation/commonActionsImpl";
import {strictEqual} from "assert";
import {browser} from "protractor";


    Then('Title {string} is displayed', async (title) => {
        await strictEqual(await new contractsPageImpl().returnContractTitle(), title);
    });

    Then('I click to edit contract with Client Name {string}', async (clientName) => {
        await new contractsPageImpl().clickToEditContract(clientName);
    });

    Then('Contracts table contains the following columns', async (dataTable) => {
        const headers = await dataTable.raw();
        const columns = await new contractsPageImpl().verifyTableHeaders();
        await new commonActionsImpl().compareArrays(columns, headers[0]);
    });

   Then ('I verify contract with Client name {string} is present', async(clientName) =>{
       browser.sleep(2000);
       await expect(await new contractsPageImpl().returnSingleContract(clientName)).to.have.lengthOf(1);
   });

    Then ('I verify contract with Client name {string} has {string} {string}', async(clientName, value, clientDetail) =>{
        browser.sleep(2000);
        await expect(await new contractsPageImpl().returnSingleContractDetails(clientName)).to.include(clientDetail);
    });