import {After, Status, When} from "cucumber";
import {contractsPageImpl} from "../pageImplementation/contractsPageImpl";
import {createContractPageImpl} from "../pageImplementation/createContractPageImpl";
import {browser} from "protractor";
import {commonActionsImpl} from "../pageImplementation/commonActionsImpl";
import {createContractPageElems} from "../pageObjectsElems/createContractPageElems";
import logger from "../util/logger.log";


    When('I navigate to create contract page', async () => {
        await new contractsPageImpl().clickBtnCreateContract();
        await browser.sleep(3000);
        await new createContractPageImpl().saveContractId();
    });

    When('I {string} the following contract details', async (string, dataTable) => {
        const dataTableValues = await dataTable.raw();
        await new createContractPageImpl().enterContractDetails(dataTableValues);
    });

    When('I {string} all networks', async (string) => {
        await new createContractPageImpl().networksSelection(string);
        await browser.sleep(2000);
    });

    When('Validation errors appear', async (docString) => {
        let expectedValidationErrors = [];
        expectedValidationErrors = docString.split('\n');
        logger.info('Val errors parsed : ' + expectedValidationErrors);
        await new commonActionsImpl().compareArrays(await new createContractPageImpl().returnValidationErrors(), expectedValidationErrors);
        await browser.sleep(2000);
    });

    When('I click on {string} button', async (string) => {
        switch(string) {
            case("Save"): {
                await new createContractPageElems().btnSave.click();
                await browser.sleep(5000);
                break;
            }
            case("Cancel"): {
                await new createContractPageElems().btnCancel.click();
                break;
            }
            case("Delete"): {
                await new createContractPageElems().btnDelete.click();
                break;
            }
        }
    });