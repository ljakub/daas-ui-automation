    import {Given, Then, When,} from "cucumber";
    import logger from "../util/logger.log";
    import {loginPageImpl} from "../pageImplementation/loginPageImpl";
    import {contractsPageElems} from "../pageObjectsElems/contractsPageElems";
    import assert from "assert";
    import {browser} from "protractor";
    import {pageTitles} from "../pageObjectsElems/pageTitles";
    import {navBarImpl} from "../pageImplementation/navBarImpl";
    import {protractorWaitUtil} from "../util/protractor_wait.util";
    import {loginPageElems} from "../pageObjectsElems/loginPageElems";
    import {navBarElems} from "../pageObjectsElems/navBarElems";



    Given('Navigate to Daas url', async () => {
        await new loginPageImpl().openBrowser();
        logger.info('Browser Opened and URL entered')
    });

    When('Login using default credentials', async () => {
        await new loginPageImpl().signIn();
    });

    When('Login using user name {string} and password {string}', async (email, password) => {
        await new protractorWaitUtil().waitForElementToBeVisible(loginPageElems.tbEmail);
        await new loginPageImpl().setEmail(email);
        await new loginPageImpl().setPassword(password);
    });

    When('I click to Sign in button', async () => {
        await new loginPageImpl().pressSignInButton();
    });

    Then('{string} validation error is', async (string, docString) => {
        switch (string) {
            case ('Password'): {
                logger.info('Password validation error' + await new loginPageImpl().passwordValidationError());
                await assert.strictEqual(await new loginPageImpl().passwordValidationError(), docString);
                break;
            }
            case ('Email'): {
                await assert.strictEqual(await new loginPageImpl().emailValidationError(), docString);
                break;
            }
            case ('Account'): {
                await assert.strictEqual(await new loginPageImpl().returnInvalidAccountError(), docString);
                await browser.sleep(2000);
                break;
            }
            default: {
                logger.info('Unknown validation error type provided: '+ string);
            }
        }
    });

    Then('Contracts & Delivery page is displayed successfully', async () => {
        await new protractorWaitUtil().waitForElementToBeVisible(new contractsPageElems().createContract);
        const title = await new pageTitles().getTitle();
        await assert.strictEqual(new pageTitles().contractsPage, title);

    });

    Then('Logout of the application', async () => {
        await new navBarImpl().logout();
        const title = await new pageTitles().getTitle();
        await assert.strictEqual(new pageTitles().contractsPage, title);
    });