import {browser} from "protractor";
import {navBarElems} from "../pageObjectsElems/navBarElems";
import {protractorWaitUtil} from "../util/protractor_wait.util";

var navBarElements = new navBarElems();

export class navBarImpl {


    async logout() {
        await browser.get(browser.baseUrl);
        browser.refresh();
        await browser.sleep(2000);
        await new protractorWaitUtil().waitForElementToBeClickable(await navBarElements.userInfoDots);
        await browser.executeScript("arguments[0].click();", await navBarElements.userInfoDots);

        await new protractorWaitUtil().waitForElementToBeClickable(await navBarElements.logout);
        await navBarElements.logout.click();
    }

}