import {protractorWaitUtil} from "../util/protractor_wait.util";
import logger from "../util/logger.log";
import {expect} from "chai";

export class commonActionsImpl {
    utilProtractorWait = new protractorWaitUtil();

    async clickActionBtn(element) {
        await this.utilProtractorWait.waitForElementToBeVisible(element);
        await this.utilProtractorWait.waitForElementToBeClickable(element);
        await element.click();
    }

    async compareArrays(arrayActualValues, arrayExpectedValues) {
        logger.info('Expected values :' + arrayExpectedValues);
        for (let i = 0; i < await arrayActualValues.length; i++) {
            if (await arrayActualValues[i].getText() !== "") {
                logger.info("Actual value beeing verified: " + await arrayActualValues[i].getText());
                await expect(arrayExpectedValues).to.include(await arrayActualValues[i].getText());
            }
        }
    }
}