import {browser} from "protractor";
import {loginPageElems} from "../pageObjectsElems/loginPageElems";
import {protractorWaitUtil} from '../util/protractor_wait.util';
import {contractsPageElems} from "../pageObjectsElems/contractsPageElems";

export class loginPageImpl {

    loginPageElems = new loginPageElems();

    async openBrowser() {
        browser.driver.manage().window().maximize();
        await browser.get(browser.baseUrl);
    }


    async setEmail(value) {
        await new protractorWaitUtil().waitForElementToBeClickable(loginPageElems.tbEmail);
        await loginPageElems.tbEmail.clear();
        await loginPageElems.tbEmail.sendKeys(value);
    }

    async setPassword(value) {
        await loginPageElems.tbPassword.clear();
        await loginPageElems.tbPassword.sendKeys(value);
    }

    async pressSignInButton() {
        await new protractorWaitUtil().waitForElementToBeClickable(loginPageElems.btnSignIn);
        await loginPageElems.btnSignIn.click();
    }

    async pressOkButton() {
        await loginPageElems.btnOK.click();
        await new protractorWaitUtil().waitForElementToBeInVisible(loginPageElems.btnOK);
    }

    async signIn() {
        await new protractorWaitUtil().waitForElementToBeVisible(loginPageElems.tbEmail);
        await this.setEmail(loginPageElems.params.signIn.email);
        await this.setPassword(loginPageElems.params.signIn.password);
        await browser.sleep(3000);
        await this.pressSignInButton();
        await browser.sleep(3000);
        await new protractorWaitUtil().waitForElementToBeVisible(new contractsPageElems().createContract);
        // await new protractorWaitUtil().waitForElementToBeVisible(loginPageElems.invalidLoginAlert);
        // await browser.waitForRouterComplete();
    }

    async signInWithParameters(email, password) {
        await this.setEmail(email);
        await this.setPassword(password);
        await this.pressSignInButton();
    }
    // async validateLogin() {
    //     await waitToBeDisplayed(await new contractsPageElems().homepageLogo,20000);
    //     await new HomePageElems().homepageLogo.isDisplayed();
    //     logger.info("Login Successful");
    // }

    async emailValidationError() {
        await browser.sleep(2000);
        return new loginPageElems().errorMessages.get(0).getText();
    }

    async passwordValidationError() {
        await browser.sleep(2000);
        return new loginPageElems().errorMessages.get(1).getText();
    }

    async returnInvalidAccountError(){
        await browser.sleep(2000);
        await new protractorWaitUtil().waitForElementToBeVisible(new loginPageElems().invalidLoginAlert);
        return new loginPageElems().invalidLoginAlert.getText();
    }
}