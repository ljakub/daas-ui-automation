import {contractsPageElems} from "../pageObjectsElems/contractsPageElems";
import {protractorWaitUtil} from "../util/protractor_wait.util";
import logger from "../util/logger.log";
import {browser, by, element} from "protractor";

let pageElems = new contractsPageElems();
export class contractsPageImpl {

    async returnContractTitle(){
        await new protractorWaitUtil().waitForElementToBeVisible(await pageElems.contractsPageHeader);
        await browser.sleep(2000);
        logger.info('Title displayed : '+ await pageElems.contractsPageHeader.getText());
        return pageElems.contractsPageHeader.getText();
    }

    async verifyTableHeaders() {
        let colmunsValues = [];
        await pageElems.contractsTableHeaders.each(async  (element) => {
            colmunsValues.push(element);
        });
        return colmunsValues;
    };


    async clickBtnCreateContract() {
        await new protractorWaitUtil().waitForElementToBeVisible(await pageElems.createContract);
        await new protractorWaitUtil().waitForElementToBeClickable(await pageElems.createContract);
        await pageElems.createContract.click();
        logger.info("Clicked 1-st time on button to create new contract.");

    }

    async clickToEditContract(contractName){
        await pageElems.contractsList.each(async  (element) => {
            if((await element.getText()).includes(contractName)){
               logger.info(await element.getText());
               element.element(by.css('td a')).click();
            }
        });
    }

    async returnSingleContract(contractName){
        let contractValues = [];
        await pageElems.contractsList.each(async  (element) => {
            if((await element.getText()).includes(contractName)){
            logger.info(await element.getText());
            contractValues.push(await element.getText());
            }
        });
        return contractValues;
    }

    async returnSingleContractDetails(contract){
        return await (await this.returnSingleContract(contract))[0].split(' ');
    }

}