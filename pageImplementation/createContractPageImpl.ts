import {protractorWaitUtil} from "../util/protractor_wait.util";
import {createContractPageElems} from "../pageObjectsElems/createContractPageElems";
import {expect} from "chai";
import logger from "../util/logger.log";
import {browser, protractor} from "protractor";
import {strictEqual} from "assert";

var contractPageElems = new createContractPageElems();
var contractIdvalue;
export default contractIdvalue

export class createContractPageImpl {

    utilProtractorWait = new protractorWaitUtil();

    async enterClientName(clientName) {
        await this.utilProtractorWait.waitForElementToBeVisible(await contractPageElems.clientName);
        await this.utilProtractorWait.waitForElementToBeClickable(await contractPageElems.clientName);
        await contractPageElems.clientName.click();
        await contractPageElems.clientName.clear();
        await contractPageElems.clientName.sendKeys(clientName);
    }

    async enterContractDetails(contractDetails) {
        for (let i = 1; i < contractDetails.length; i++) {
            logger.info('Field :' + contractDetails[i][0]);
            switch (contractDetails[i][0]) {
                case('Client Name'): {
                    await this.enterClientName(contractDetails[i][1]);
                    logger.info("Client name: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    const clientNameAct = await contractPageElems.clientNameText.getAttribute('value');
                    expect(clientNameAct).to.include(contractDetails[i][1]);
                    break;
                }
                case('Client Type'): {
                    await this.displayDropdown(contractPageElems.clientType);
                    await this.selectValueFromDropdown(contractPageElems.clientType, contractDetails[i][1]);

                    logger.info("Client Type value: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Data Feed Type'): {
                    await this.displayDropdown(contractPageElems.dataFeedType);
                    await this.selectValueFromDropdown(contractPageElems.dataFeedType, contractDetails[i][1]);

                    logger.info("Data Feed Type value: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Delivery Cadence'): {
                    await this.displayDropdown(contractPageElems.deliveryCadence);
                    await this.selectValueFromDropdown(contractPageElems.deliveryCadence, contractDetails[i][1]);

                    logger.info("Delivery Cadence value: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Delivery Location'): {
                    await this.displayDropdown(contractPageElems.deliveryLocation);
                    await this.selectValueFromDropdown(contractPageElems.deliveryLocation, contractDetails[i][1]);

                    logger.info("Delivery Location value: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Final ID Type Delivered'): {
                    await this.displayDropdown(contractPageElems.finalIDType);
                    await this.utilProtractorWait.waitForElementToBeVisible(await contractPageElems.finalIDTYpeInput);
                    await contractPageElems.finalIDTYpeInput.sendKeys(contractDetails[i][1]);
                    browser.actions().sendKeys(protractor.Key.ENTER).perform();

                    logger.info("Final ID Type Delivered value: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Start Date'): {
                    await this.displayDropdown(contractPageElems.startDate);
                    await this.enterDate(contractPageElems.startDateInput, contractDetails[i][1]);

                    logger.info("Start Date: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('End Date'): {
                    await this.displayDropdown(contractPageElems.endDate);
                    await this.enterDate(contractPageElems.endDateInput, contractDetails[i][1]);

                    logger.info("Start Date: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('Initial Delivery Date'): {
                    await this.displayDropdown(contractPageElems.initialDeliveryDate);
                    await this.enterDate(contractPageElems.initialDeliveryDateInput, contractDetails[i][1]);

                    logger.info("Start Date: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }
                case('UPCs'):{
                    // await this.enterDate(contractPageElems.upcSelection,contractDetails[i][1]);
                    // await browser.actions().sendKeys(protractor.Key.ENTER).perform();
                    await contractPageElems.upcSelection.clear();
                    await contractPageElems.upcSelection.sendKeys(contractDetails[i][1]);

                    logger.info("UPC's: " + contractDetails[i][1]);
                    await browser.sleep(2000);
                    break;
                }

                default: {
                    logger.info('No detail have been recognised.')
                }
                    ;
            }

        }
    }

    private async enterDate(element, value) {
        await browser.actions().sendKeys(protractor.Key.ENTER).perform();
        await element.clear();
        await element.sendKeys(value);
    }

    private async displayDropdown(fieldType) {
        await this.utilProtractorWait.waitForElementToBeClickable(await fieldType);
        await fieldType.click();
    }

    private async selectValueFromDropdown(fieldType, value) {
        logger.info('Drop-down to select value opened: ' +await fieldType.getAttribute('aria-expanded'));
        await strictEqual(await fieldType.
        getAttribute('aria-expanded'),'true',fieldType + 'drop-down was not opened.');

        logger.info('Waiting for dropdown values to be present');
        await this.utilProtractorWait.waitForElementToBeClickable(await contractPageElems.dropdownValuesForm);

        await contractPageElems.dropdownValues.each(async (element) =>{
            let myValue = await element.getText();
            if(myValue === value){
                logger.info('Value to be selected : ' + myValue);
                element.click();
                // await strictEqual(await contractPageElems.clientTypeText.getText(), value, 'Expected in selection');
            }
        });
    }

    async networksSelection(option){
        if(option === 'select'){
        await this.utilProtractorWait.waitForElementToBeClickable(await contractPageElems.selectNetworks);
        await browser.sleep(3000);
        await contractPageElems.selectNetworks.click();
        }
        else if (option === 'deselect'){
            await this.utilProtractorWait.waitForElementToBeClickable(await contractPageElems.deselectNetworks);
            await browser.sleep(2000);
            await contractPageElems.deselectNetworks.click();
            await browser.sleep(3000);
        }
        else logger.info('Unknown selection options.')

    }

    async returnValidationErrors(){
            let validationErrors = [];
            await contractPageElems.validationErrors.each(async (element) => {
                await validationErrors.push(element);
            });
            logger.info('Returned validation errors: '+ validationErrors.values());
        return validationErrors;

     }

    async saveContractId(){
        contractIdvalue =  new createContractPageElems().contractId.getText();
    }

    async returnContractId(){
        return contractIdvalue;
    }

}
