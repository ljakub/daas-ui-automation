import {browser} from "protractor";

exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['--window-size=1920,1080', '--no-sandbox']
    }
  },

// This can be changed via the command line as: --params.login.user 'ngrocks'
  params: {
    signIn: {
      email: 'lenka.jakubecova@catalina.com',
      password: 'Karol2020*'
    }
  },

  baseUrl: 'http://digital-daas-dashboard-om-sqa.nplabsusk8s.catmktg.com/',
  // baseUrl: 'http://localhost:9000',

  framework: 'custom',  // set to "custom" instead of cucumber.

  frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

  // Specs here are the cucumber feature files
  specs: [
    '.src/features/*.feature'
  ],

  // Disable Control Flow, it will be removed in future WebDriverJS version
  SELENIUM_PROMISE_MANAGER: false,


  // cucumber command line options
  cucumberOpts: {
    require: ['.src/steps/*.js'],  // require step definition files before executing features
    tags: [],                      // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                  // <boolean> fail if there are any undefined or pending steps
    format: ["pretty"],            // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    'dry-run': false,              // <boolean> invoke formatters without executing steps
    compiler: []                   // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  },


  onPrepare: function() {
    browser.manage().window().maximize();
  }
};
