import {by, element} from 'protractor';

export class createContractPageElems {

    btnCancel = element(by.css('#cancelBtn'));
    btnSave = element(by.css('#saveBtn'));
    btnDelete = element(by.css('#deleteBtn'));

    selectNetworks = element(by.css('#selectallnets'));
    deselectNetworks = element(by.css('#deselectallnets'));
    validationErrors = element.all(by.css('#pmd-main div > div > ul > li'));

    contractId = element(by.css('#pmd-main  ol > li.active'));

    //Client Name
    clientName = element(by.css('input[id=clientName]'));
    clientNameText = element(by.css('div.pmd-card-body > div:nth-child(1) > div:nth-child(1) > div > input'));

    dropdownValuesForm = element(by.css('span.select2-dropdown.select2-dropdown--below > span:nth-child(2) > ul'));
    dropdownValues = element.all(by.css('span.select2-dropdown.select2-dropdown--below > span:nth-child(2) > ul > li'));

    //Client Type
    clientType = element(by.css('select-dropdown[id=clientType] > span.select2.select2-container.select2-container--bootstrap > span.selection > span'));
    clientTypeText = element(by.css('select-dropdown[id=clientType] > span > span > span > span'));

    //Data Feed Type
    dataFeedType = element(by.css('select-dropdown[id=dataType] > span.select2.select2-container.select2-container--bootstrap > span.selection > span'));

    //Delivery Cadence
    deliveryCadence = element(by.css('select-dropdown[id=cadence] > span.select2.select2-container.select2-container--bootstrap > span.selection > span '));

    //Start Date
    startDate = element(by.css('date[id=startDate] > div'));
    startDateInput = element(by.css('date[id=startDate] input'));

    //End Date
    endDate = element(by.css('date[id=endDate] > div'));
    endDateInput = element(by.css('date[id=endDate] input'));

    //Delivery location
    deliveryLocation = element(by.css('select-dropdown[id=locations] > span.select2.select2-container.select2-container--bootstrap > span.selection > span '));

    //Final ID Type
    finalIDType = element(by.css('select-dropdown[id=finalIDType] > span.select2.select2-container.select2-container--bootstrap > span.selection > span '));
    finalIDTYpeInput = element(by.css('span.select2-container.select2-container--bootstrap.select2-container--open > span > span > input'));

    //Initial Delivery Date
    initialDeliveryDate = element(by.css('date[id=initialDeliveryDate] > div'));
    initialDeliveryDateInput = element(by.css('date[id=initialDeliveryDate] input'));

    //UPCs
    upcSelection = element(by.css('textarea[id=upc6]'));
}