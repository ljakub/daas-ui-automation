import {element, by, browser} from "protractor";

export class loginPageElems{
    static tbEmail = element(by.css('div.pmd-card-body > div:nth-child(1) > div > input'));
    static tbPassword = element(by.css('div.pmd-card-body > div:nth-child(2) > div > input'));
    static btnSignIn = element(by.css('body > div.body-custom > div > div > div > form > div.pmd-card-footer.card-footer-no-border.card-footer-p16.text-center > button'));
    invalidLoginAlert = element(by.css('body > div.uk-modal.uk-open > div > div.uk-modal-body'));
    static btnOK = element(by.css('body > div.uk-modal.uk-open > div > div.uk-modal-footer.uk-text-right > button'));
    errorMessages = element.all(by.css('form div.uk-text-danger'));
    // utilProtractorWait = new ProtractorWaitUtil();
    static params = browser.params;
}