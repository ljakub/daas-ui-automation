import {browser} from "protractor";

export class pageTitles {
        loginPage = 'DAAS Dashboard';
        contractsPage = 'Contracts & Delivery | Contracts';

        async getTitle(){
                const title = await browser.getTitle();
                return title;
        }
}