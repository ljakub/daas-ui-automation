import {by, element} from 'protractor';
import 'regenerator-runtime/runtime';

export class contractsPageElems {
       createContract = element(by.css('#services > a > i'));
       contractsTableHeaders = element.all(by.css('table[id=DataTables_Table_0] tr > th'));
       contractsPageHeader = element(by.css('.section-title > span'));
       contractsList = element.all(by.css('table tr'));

 }


