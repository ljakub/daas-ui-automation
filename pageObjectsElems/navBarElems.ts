import {element, by, browser} from "protractor";

export class navBarElems{
    userInfoDots = element(by.css('div.dropdown.pmd-dropdown.pmd-user-info.pull-right > a > div.media-right.media-middle > i'));
    editProfile = element(by.css('#editProfile'));
    logout = element(by.css('#logout'));
    navBarHeader = element(by.css('div.navbar-header > a > i'));
    // static invalidLoginAlert = element(by.css('body > div.uk-modal.uk-open > div > div.uk-modal-body'));
    // static btnOK = element(by.css('body > div.uk-modal.uk-open > div > div.uk-modal-footer.uk-text-right > button'));
    // static errorMessages = element.all(by.css('form div.uk-text-danger'));
    // // utilProtractorWait = new ProtractorWaitUtil();
    static params = browser.params;
}